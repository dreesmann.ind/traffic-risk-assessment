import pandas as pd
import streamlit as st
from traffic_risk_assessment import CONFIG


def setup():
    if st.session_state.get("RAW_DATA") is None and CONFIG.ENVIRONMENT == "DEV":
        df = (
            pd.read_csv(CONFIG.ROUTES_CSV_PATH)
            .sort_values(by="distance", ascending=False)
            .head(10)
            .reset_index()
        )
        st.session_state["RAW_DATA"] = df
