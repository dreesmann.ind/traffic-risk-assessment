from .home.app import main as home
from .overview.app import main as overview
from .historical.app import main as historical
from .live.app import main as live
from .eda.app import main as eda
