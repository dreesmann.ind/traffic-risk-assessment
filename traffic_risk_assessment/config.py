import dotenv
import os
import pathlib
from dataclasses import dataclass

dotenv.load_dotenv(dotenv.find_dotenv())


@dataclass
class Configuration:
    ENVIRONMENT=os.environ.get("TRAFFIC_RISK_ENVIRONMENT") 
    HOME = pathlib.Path.home() / ".traffic_risk_assessment"
    CACHE_DIR = HOME / "cache"
    HERE_API_KEY = os.environ.get("HERE_API_KEY")
    HERE_APP_ID = os.environ.get("HERE_APP_ID")
    TOMTOM_API_KEY = os.environ.get("TOMTOM_API_KEY")
    MAPBOX_API_TOKEN = os.environ.get("MAPBOX_API_TOKEN")

    DATA_PATH = pathlib.Path(__file__).parent / "data"
    ROUTES_CSV_PATH = DATA_PATH / "Routen/routes.csv"
    HISTORICAL_INCIDENTS_PATH = DATA_PATH / "Staudaten"


CONFIG = Configuration()
