# traffic-risk-assessment

To run the dashboard, python 3.8.10 and [poetry]() need to be installed. To install dependencies run:
```bash
make install
```
To run the dashboard use 
```bash
make dashboard
```

What you need before starting to use this project:
- here.com API-Key
- tomtom.com API-Key

-> Use the example.env as a template for your .env file and fill in your api-keys