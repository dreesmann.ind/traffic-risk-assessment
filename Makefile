install:
	poetry install
dashboard:
	poetry shell && streamlit run app.py
notebook:
	poetry shell && cd traffic-risk-assessment/notebook && jupyterlab
test:
	poetry shell && pytest
all: install test