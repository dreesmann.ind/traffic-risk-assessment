import mimetypes
import streamlit as st
import pandas as pd
from traffic_risk_assessment import CONFIG
from traffic_risk_assessment.dashboard import shared


def main():
    shared.setup()
    st.write(
        """
Welcome to the Route Analysis Dashboard. Navigate the different sub-dashboards via the Menu on the left side :arrow_backward:.
Upload a .csv in the following format to analyse your routes. You can download the example data below to fill out your own csv file.
If you dont upload a .csv, an example analysis will be shown.
"""
    )
    df = st.session_state["RAW_DATA"]
    st.dataframe(df.head())
    st.download_button(
        "⬇️ Download example csv",
        df.head().to_csv(index=False).encode("utf-8"),
        file_name="example.csv",
    )

    uploaded_file = st.file_uploader("Upload your csv here:", type=".csv")
    if uploaded_file:
        df = pd.read_csv(uploaded_file)
        if CONFIG.ENVIRONMENT == "DEV":
            df = df.head(100)
        st.session_state["RAW_DATA"] = df
    st.write(
        """
    - Under *Risk Overview* you find an overall analysis of risk on your routes.
    - Further you can find historical information under the *Historical Analysis* dashboards
    """
    )


if __name__ == "__main__":
    main()
