import pandas as pd
import streamlit as st
import traffic_risk_assessment
from traffic_risk_assessment.dashboard import streamlit_utils


def main():
    st.sidebar.title(":computer: Dashboard Menu")

    streamlit_utils.st_query_radio(
        "",
        "p",
        {
            "Upload dataset": traffic_risk_assessment.dashboard.home,
            "Risk Overview": traffic_risk_assessment.dashboard.overview,
            # "Exploratory Analysis": traffic_risk_assessment.dashboard.eda,
            "Historical Analysis": traffic_risk_assessment.dashboard.historical,
            # "Live Analysis": traffic_risk_assessment.dashboard.live,
        },
    )()


if __name__ == "__main__":

    st.set_page_config(page_title="T-RAD", page_icon=":bar_chart:", layout="wide")
    st.title(":truck: Truck route analysis dashboard")

    main()
