import io
import pathlib
import typing as t
import pandas as pd
import geopandas as gpd
from here_location_services import LS
from here_location_services.config import dest_weather_config

import herepy
from shapely.geometry import LineString, Polygon
import diskcache as dc
from traffic_risk_assessment import CONFIG
import logging

logger = logging.getLogger(__name__)
cache = dc.Cache(CONFIG.CACHE_DIR)

HERE_LOCATION_SERVICE = LS(api_key=CONFIG.HERE_API_KEY)
HERE_TRAFFIC_API = herepy.TrafficApi(CONFIG.HERE_API_KEY)


@cache.memoize(tag="HERE")
def get_weather_for_route(route: LineString):
    coords = list(route.coords)
    if len(coords[0]) == 3:
        # deal with elevation
        coords = [x[:2] for x in coords]
    start = coords[0]
    mid = coords[len(coords) // 2]
    dest = coords[-1]

    start_weather = HERE_LOCATION_SERVICE.get_dest_weather(
        at=start, products=[dest_weather_config.DEST_WEATHER_PRODUCT.forecastHourly]
    )
    start_weather = pd.json_normalize(
        start_weather.response.get("places")[0]
        .get("hourlyforecasts")[0]
        .get("forecasts")
    )

    mid_weather = HERE_LOCATION_SERVICE.get_dest_weather(
        at=mid, products=[dest_weather_config.DEST_WEATHER_PRODUCT.forecastHourly]
    )
    mid_weather = pd.json_normalize(
        mid_weather.response.get("places")[0].get("hourlyforecasts")[0].get("forecasts")
    )

    dest_weather = HERE_LOCATION_SERVICE.get_dest_weather(
        at=dest, products=[dest_weather_config.DEST_WEATHER_PRODUCT.forecastHourly]
    )
    dest_weather = pd.json_normalize(
        dest_weather.response.get("places")[0]
        .get("hourlyforecasts")[0]
        .get("forecasts")
    )

    return start_weather, mid_weather, dest_weather


def load_weather_for_routes(routes_df) -> dict:
    route_to_weather = {}
    for index, row in routes_df.iterrows():
        weather = get_weather_for_route(row.geometry)
        route_to_weather[index] = weather
    return route_to_weather


@cache.memoize(tag="HERE")
def request_here_traffic_incidents(route: LineString, corridor_width: int = 1000):
    """Request incidents from here.com. There is a limit on how many route can be included, but it's not mentioned in the api docs. Therefore it's capped to 10."""
    coords = list(route.coords)
    if len(coords[0]) == 3:
        # deal with elevation
        coords = [x[:2] for x in coords]
    route = [list(reversed(x[:2])) for x in coords]
    if len(route) > 10:
        corridor = route[slice(0, len(route), len(route) // 10)]
    response = HERE_TRAFFIC_API.incidents_in_corridor(corridor, corridor_width)
    return response


@cache.memoize(tag="HERE")
def request_here_traffic_flow(waypoints: t.List, corridor_width: int = 1000):
    """Request traffic flow from here.com. There is a limit on how many waypoints can be included, but it's not mentioned in the api docs. Therefore it's capped to 10."""

    waypoints = [list(reversed(x[:2])) for x in waypoints]
    if len(waypoints) > 10:
        corridor = waypoints[slice(0, len(waypoints), len(waypoints) // 10)]
    response = HERE_TRAFFIC_API.flow_in_corridor(corridor, corridor_width)
    return response
