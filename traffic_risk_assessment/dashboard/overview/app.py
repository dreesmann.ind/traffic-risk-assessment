import streamlit as st

from streamlit_folium import folium_static
import pandas as pd
import geopandas as gpd
from traffic_risk_assessment.dashboard import maps, routing, shared, context_information
import logging

logger = logging.getLogger(__name__)


@st.cache
def load_data():
    shared.setup()
    origin, destination = routing.routes_from_csv(st.session_state["RAW_DATA"])
    here_routes, mapbox_routes = routing.load_routes(st.session_state["RAW_DATA"])
    st.session_state["HERE_ROUTES"] = here_routes
    st.session_state["MAPBOX_ROUTES"] = mapbox_routes
    if st.session_state.get("WEATHER") is None:
        st.session_state["WEATHER"] = context_information.load_weather_for_routes(
            st.session_state["HERE_ROUTES"]
        )

    merged = origin.merge(destination, left_index=True, right_index=True)
    merged = here_routes.merge(
        merged,
        how="left",
        left_on=["origin_lat", "origin_lon", "destination_lat", "destination_lon"],
        right_on=["lat_x", "lon_x", "lat_y", "lon_y"],
    )
    # merged enthält origin, destination und die routen dazwischen.
    st.session_state["MERGED"] = merged

    return origin, destination, here_routes, mapbox_routes


def map_top_10_distances(
    origin: pd.DataFrame, destination: pd.DataFrame, sortby="distance"
):
    top_10 = origin.sort_values(sortby, ascending=False)
    top_10 = pd.merge(
        top_10.head(10), destination["destination"], left_index=True, right_index=True
    )
    logger.info(top_10)
    with st.container():
        col1, col2 = st.columns(2)
        col1.write("Top 10 longest routes by Distance")
        col1.write(top_10[["origin", "destination", "distance"]])
        origin = origin.iloc[top_10.index]
        destination = destination.iloc[top_10.index]
        merged = pd.merge(origin, destination, left_index=True, right_index=True)

        deck = maps.great_circle_origin_destination(merged)
        col2.pydeck_chart(deck)


def get_incidents_kpi(routes):
    incidents = routes.incidents.dropna()
    incidents = pd.json_normalize(incidents)
    criticality_counts = {}
    for idx, row in incidents.iterrows():
        for col in row:
            if type(col) == dict:
                level = col.get("criticality")
                if criticality_counts.get(level) is None:
                    criticality_counts[level] = 1
                else:
                    criticality_counts[level] += 1
    return criticality_counts


def get_duration_kpis(routes):
    df = routes.dropna(subset=["duration", "base_duration"])
    avg_duration = df.duration.mean() / 60
    avg_base_duration = df.base_duration.mean() / 60
    return avg_base_duration, avg_duration


def get_distance_kpi(routes):
    df = st.session_state.get("MERGED").copy()
    lengths = df.groupby(["origin", "destination"])["length"].min()
    min_dist = lengths.sum() / 1000
    lengths = df.groupby(["origin", "destination"])["length"].max()
    max_dist = lengths.sum() / 1000
    return min_dist, max_dist
    # st.write(lengths)


def get_most_departures(routes):
    return routes.origin.value_counts().index.to_list()[0]


def get_most_arrivals(routes):
    return routes.destination.value_counts().index.to_list()[0]


def kpis(routes, merged):
    mcol1, mcol2, mcol3, mcol4 = st.columns(4)
    incident_counts = get_incidents_kpi(routes)
    for key, count in incident_counts.items():
        mcol1.metric(f"{key.upper()} incidents on routes", count)
    base_dur, dur = get_duration_kpis(routes)
    mcol2.metric(
        f"Average Base duration across routes",
        "{:.2f} h".format(base_dur),
    )
    mcol2.metric(
        f"Average duration due to incidents across routes",
        "{:.2f} h".format(dur),
        "-{:.2f} h".format(dur - base_dur),
    )
    min_dist, max_dist = get_distance_kpi(routes)
    mcol3.metric("Shortest routes total driven Km", "{:.2f}".format(min_dist))
    mcol3.metric("Longest routes total driven Km", "{:.2f}".format(max_dist))
    mcol4.metric(
        "Most departures",
        get_most_departures(merged),
    )
    mcol4.metric(
        "Most arrivals",
        get_most_arrivals(merged),
    )


def main():
    origin, destination, here_routes, mapbox_routes = load_data()

    st.write("## :telescope: Overview")
    st.write("## Risk KPIS:")
    kpis(here_routes.copy(), st.session_state["MERGED"].copy())

    col1, col2 = st.columns(2)
    col1.write("### Origins:")
    col1.write(origin)
    col2.write("### Destinations:")
    col2.write(destination)
    st.write("### Origin Cities in green, destination cities in red:")

    st.pydeck_chart(maps.scatter_origin_destination(origin, destination))
    st.write("## 🗺️ Routes:")
    st.dataframe(st.session_state["MERGED"].drop(columns="geometry"))
    st.download_button(
        "Download routes for your Origin-Destination pairs",
        st.session_state["MERGED"].drop(columns="geometry").to_csv().encode("utf-8"),
        "routes.csv",
    )


if __name__ == "__main__":
    main()
